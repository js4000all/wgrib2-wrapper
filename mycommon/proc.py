from subprocess import Popen, PIPE, STDOUT

class PopenContext:
  @staticmethod
  def as_text_generator(cmd, from_stderr=False):
    sink_stdout, sink_stderr = (STDOUT, PIPE) if from_stderr else (PIPE, STDOUT) 
    return _TextGeneratingProcess(cmd, sink_stdout, sink_stderr)

  @staticmethod
  def as_binary_generator(cmd):
    return _BinaryGeneratingProcess(cmd)

class _TextGeneratingProcess:
  def __init__(self, cmd, sink_stdout, sink_stderr):
    self.cmd = cmd
    self.sink_stdout = sink_stdout
    self.sink_stderr = sink_stderr

  def execute(self):
    with Popen(self.cmd, stdout=self.sink_stdout, stderr=self.sink_stderr, text=True) as p:
      for line in p.stdout:
        yield line.strip()

class _BinaryGeneratingProcess:
  def __init__(self, cmd):
    self.cmd = cmd

  def execute(self, bufsize=4096):
    buf = bytearray(bufsize)
    with Popen(self.cmd, stdout=PIPE, stderr=STDOUT) as p:
      while (n := p.stdout.readinto(buf)) > 0:
        yield buf[:n] if n < bufsize else buf

