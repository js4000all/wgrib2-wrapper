import sys
from contextlib import contextmanager
from wwgrib2.wgrib2_wrapper import Wgrib2Wrapper

def main(infiles):
  ww = Wgrib2Wrapper("bin/wgrib2")
  cx = ww.make_context()

  for path in infiles:
    cx.add_grib2_path(path)

  for name, field in cx.fields().items():
    print(f'{name}')
    print(f'  ({field.fullname()}) [{field.unit()}] {field.level()}')
    print(f'  (n={len(field.metadata())})')
    for md in field.metadata():
      print(f'  {md.forecast_hours()} -> {md.key()}@{md.src()} ({md.initial_time()})')

  print(" -- ")

  for g in cx.fields()["TMP"].metadata()[0].grids():
    print(f'{g.time0()}/{g.time1()}/{g.lng()}/{g.lat()}/{g.value()}')
  print("[root] -- Complete --")
  
if __name__ == "__main__":
  infiles = sys.argv[1:]
  main(infiles)


