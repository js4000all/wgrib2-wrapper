from collections import defaultdict
import re
from mycommon.proc import PopenContext

class Wgrib2Wrapper:
  def __init__(self, wgrib2_bin_path):
    self._wgrib2_bin_path = wgrib2_bin_path

  def make_context(self):
    return _Context(self._wgrib2_bin_path)


class _Context:
  def __init__(self, wgrib2_bin_path):
    self._wgrib2_bin_path = wgrib2_bin_path
    self._grib2s = []

  def add_grib2_path(self, path):
    self._grib2s.append(_Grib2(self._wgrib2_bin_path, path))

  def metadata_all(self):
    for grib2 in self._grib2s:
      for metaWithSrc in grib2._get_metadataWithSource():
        yield metaWithSrc
  
  def fields(self):
    m = defaultdict(_Field)
    for metaWithSrc in self.metadata_all():
      m[metaWithSrc._field()]._append(metaWithSrc)
    return m


class _Grib2:
  def __init__(self, wgrib2_bin_path, grib2_path):
    self._wgrib2_bin_path = wgrib2_bin_path
    self._path = grib2_path

  def _execute_wgrib2_as_lines(self, from_stderr, *options):
    cmd = [self._wgrib2_bin_path, self._path, *options]
    cx = PopenContext.as_text_generator(cmd, from_stderr)
    return cx.execute()

  def _get_metadataWithSource(self):
    for line in self._execute_wgrib2_as_lines(False, "-v"):
      meta = _Metadata._parse(line)
      yield _MetadataWithSource(meta, self)


class _Metadata:
  _FIELD_PTN = re.compile(r'([^ ]+) (.+) \[(.+)\]') 
  
  @staticmethod
  def _parse(line):
    data = line.split(":")
    key = data[0]
    initial_time = data[2][2:]
    field = data[3]
    m = _Metadata._FIELD_PTN.match(field)
    field = m.groups()[0]
    field_full = m.groups()[1]
    unit = m.groups()[2]
    level = data[4]
    kind = data[5]
    #forecast_hours = 'anl' if kind == 'anl' else kind.split(' ')[0]
    return _Metadata(key, initial_time, field, field_full, unit, level, kind)

  def __init__(self, key, initial_time, field, field_full, unit, level, forecast_hours):
    self._key = key
    self._initial_time = initial_time
    self._field = field
    self._field_full = field_full
    self._unit = unit
    self._level = level
    self._forecast_hours = forecast_hours


class _MetadataWithSource:
  def __init__(self, meta, grib2):
    self._meta = meta
    self._grib2 = grib2

  def key(self):
    return self._meta._key
  def src(self):
    return self._grib2._path
  def initial_time(self):
    return self._meta._initial_time
  def _field(self):
    return self._meta._field
  def _field_full(self):
    return self._meta._field_full
  def _unit(self):
    return self._meta._unit
  def _level(self):
    return self._meta._level
  def forecast_hours(self):
    return self._meta._forecast_hours
  def is_forecast(self):
    return self.get_forecast_hours() != 'anl'

  def grids(self):
    grid = _Grid()
    for csvrow in self._grib2._execute_wgrib2_as_lines(True, "-d", self.key(), "-csv", "/dev/stderr"):
      yield grid._set_buf(csvrow.split(','))
    

class _Field:

  def __init__(self):
    self._metaWithSrc_list = []

  def _append(self, metaWithSrc):
    self._metaWithSrc_list.append(metaWithSrc)

  def _meta1(self):
    return self._metaWithSrc_list[0]

  def metadata(self):
    return self._metaWithSrc_list
    
  def name(self):
    return self._meta1()._field()
  def fullname(self):
    return self._meta1()._field_full()
  def unit(self):
    return self._meta1()._unit()
  def level(self):
    return self._meta1()._level()


class _Grid:
  def _set_buf(self, buf):
    self._buf = buf
    return self

  def time0(self):
    return self._buf[0].strip('"')
  def time1(self):
    return self._buf[1].strip('"')
  def lng(self):
    return float(self._buf[4]) 
  def lat(self):
    return float(self._buf[5]) 
  def value(self):
    return float(self._buf[6]) 

